const express = require("express");
const app = express();
const multer = require("multer");
const path = require("path");
const bodyParser = require("body-parser");
const port = 3000;
app.use(bodyParser.json({ limit: "500mb" }));
// parsing-req.body-urlQueries-into-query-string
app.use(bodyParser.urlencoded({ limit: "500mb", extended: false }));
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./images");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "__" + file.originalname);
  },
});
const upload = multer({ storage: storage });

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "index.html"));
});
app.post("/single", upload.single("image"), (req, res) =>
  res.send("file uploaded")
);
app.post("/multiple", upload.array("images", 3), (req, res) =>
  res.send("file uploaded")
);
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
